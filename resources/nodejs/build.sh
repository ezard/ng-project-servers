#!/bin/bash

# Bash "strict mode".
# http://redsymbol.net/articles/unofficial-bash-strict-mode/ for
# details.
set -euo pipefail

export DEBIAN_FRONTEND=noninteractive

npm install

npm run build