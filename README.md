Start project
====

First see project `docker-ng-cli` README.md for help on using the docker container to run this project.

/itemate/code$ ng new udemy-ui --routing=false --style=css --skip-git --directory=angular

cd angular && npm install @angular/material @angular/cdk @angular/animations @angular/flex-layout


Host project
```
ng serve --host 0.0.0.0 --poll 4000
```
